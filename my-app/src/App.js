import moment from 'moment';
import axios from 'axios';
import aws4Interceptor from 'aws4-axios';
import React, { useState, useEffect } from 'react';
import { Line, Bar } from 'react-chartjs-2';

const deviceID = 'b176cf1e-a537-46c9-8ae7-e7b27a3de5c7';
// const deviceID = '2befffc7-acbb-4c9a-b10a-9418f52fc10e';
const start = '1632326400';
const end = '1631792896';

// const deviceId = 'c4af4daa-15fc-45fd-a922-e539f2c361f1';
const axiosSolar = {
	baseURL: 'https://9ry30zin92.execute-api.ap-southeast-1.amazonaws.com/public',
	apiKey: 'eJzWdiiQi18jzBIWxqFHV1TRAgfcIg8h8Lc0wgBB',
	region: 'ap-southeast-1',
	service: 'execute-api',
	accessKeyId: 'AKIAVBSYQGXZE6POVWVK',
	secretAccessKey: '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',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: { 'x-api-key': axiosSolar.apiKey },
});
const interceptor = aws4Interceptor(
	{
		region: axiosSolar.region,
		service: axiosSolar.service,
	},
	{
		accessKeyId: axiosSolar.accessKeyId,
		secretAccessKey: axiosSolar.secretAccessKey,
	}
);
axiosMainHelper.interceptors.request.use(interceptor);
const DynamicChart = () => {
	const [chartData, setChartData] = useState({});
	const [employeeSalary, setEmployeeSalary] = useState([]);
	const [employeeAge, setEmployeeAge] = useState([]);

	const Chart = () => {
		let empSal = [];
		let empAge = [];
		let moisture = [];
		let quotient = [];
		let temperature = [];
		let timestamp = [];

		axiosMainHelper
			.get(
				`/rawtmpdata/?deviceID=${deviceID}&start_time=${start}000&end_time=${moment().unix()}000`
				// `/rawtmpdata/?deviceID=${deviceId}`
			)
			.then((res) => {
				console.log('res', res);
				for (const row in res.data) {
					try {
						let eachDataPoint = JSON.parse(
							res.data[`${row}`].stringifiedData.S
						);
						console.log('eachDataPoint', eachDataPoint);
						if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor[0].hasOwnProperty('quotient') &&
							eachDataPoint.sensor[0].hasOwnProperty('moisture') &&
							eachDataPoint.sensor[0].hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor[0]', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor[0].moisture);
							quotient.push(eachDataPoint.sensor[0].quotient);
							temperature.push(eachDataPoint.sensor[0].temperature);
							console.log('moisture', moisture);
							// console.log('timestamp', timestamp);
						} else if (
							eachDataPoint.hasOwnProperty('sensor') &&
							eachDataPoint.sensor.hasOwnProperty('quotient') &&
							eachDataPoint.sensor.hasOwnProperty('moisture') &&
							eachDataPoint.sensor.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.sensor.moisture);
							quotient.push(eachDataPoint.sensor.quotient);
							temperature.push(eachDataPoint.sensor.temperature);
							console.log('moisture', moisture);
						} else if (
							eachDataPoint.hasOwnProperty('quotient') &&
							eachDataPoint.hasOwnProperty('moisture') &&
							eachDataPoint.hasOwnProperty('temperature')
						) {
							console.log('eachDataPoint sensor', eachDataPoint);
							timestamp.push(
								`${eachDataPoint.timestamp}_${moment
									.unix(eachDataPoint.timestamp)
									.format('dddd, MMMM Do, YYYY h:mm:ss A')}`
							);
							moisture.push(eachDataPoint.moisture);
							quotient.push(eachDataPoint.quotient);
							temperature.push(eachDataPoint.temperature);
							console.log('moisture', moisture);
						}
					} catch (e) {
						console.log(e);
					}
				}
				// const data = JSON.parse(res.data[`${row}`].stringifiedData.S);
				// for (const dataObj of res) {
				// 	empSal.push(parseInt(dataObj.employee_salary));
				// 	empAge.push(parseInt(dataObj.employee_age));
				// }
				setChartData({
					labels: timestamp,
					datasets: [
						{
							label: 'Moisture',
							data: moisture,
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
								'rgba(255, 206, 86, 0.2)',
								'rgba(75, 192, 192, 0.2)',
								'rgba(153, 102, 255, 0.2)',
								'rgba(255, 159, 64, 0.2)',
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
								'rgba(255, 99, 132, 1)',
								'rgba(54, 162, 235, 1)',
								'rgba(255, 206, 86, 1)',
								'rgba(75, 192, 192, 1)',
								'rgba(153, 102, 255, 1)',
								'rgba(255, 159, 64, 1)',
							],
							borderWidth: 1,
						},
					],
				});
			})
			.catch((err) => {
				console.log(err);
			});
	};
	useEffect(() => {
		Chart();
	}, []);
	return (
		<div className="App">
			<h1>Moisture reading of b176cf1e-a537-46c9-8ae7-e7b27a3de5c7</h1>
			<div>
				<Line
					data={chartData}
					options={{
						responsive: true,
						title: { text: 'Moisture', display: true },
						scales: {
							yAxes: {
								ticks: {
									beginAtZero: true,
								},
							},
						},
					}}
				/>
			</div>
		</div>
	);
};

export default DynamicChart;
